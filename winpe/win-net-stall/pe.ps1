﻿echo "Win-Net-Stall WindowsPE Setup 1.0"
$ErrorActionPreference = "Stop"

$BASE_PATH = "X:\Temp"
$SETUP_PATH = "X:\win-net-stall"

function Test-Windows-PE()
{
    return Test-Path -Path Registry::HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlset\Control\MiniNT
}

if (-Not(Test-Windows-PE))
{
    throw "Win-Net-Stall is not running in WindowsPE! Aborting execution!"
}

. "$SETUP_PATH\include\*.ps1"


echo "Creating temp folder"
New-Item $BASE_PATH -ItemType Directory -Force
cd $BASE_PATH

echo "Initializing network"
wpeinit

echo "Waiting for network"

# Yes, sometimes this takes 20 seconds in windows pe. I tried it with 10 and there where a ton of failed attempts...
Wait-For-Network(20)

$POST_SETUP_SCRIPTS = @() # script will add those scripts
Copy-And-Include-Specific-Config

if ($BLOCK_PERIPHERALS)
{
    Block-Peripherals
}


echo "Synchronizing time"
net time \\$TIME_SERVER /set /yes


Mount-Setup-Share


echo "Copying unattended.xml"
copy "S:\$UNATTENDED_BASE_PATH\$UNATTENDED_PATH" "$BASE_PATH\unattended.xml"

echo "Tweaking unattended.xml"

$variables = Get-Variable * -Scope Global

$unattended = Get-Content -Path "$BASE_PATH/unattended.xml" -Encoding utf8 -Raw

foreach ($script in $variables.GetEnumerator())
{
    if (-Not$( $script.Name -cmatch "^[A-Z_0-9]*$" )) # written in caps
    {
        continue
    }
    $unattended = $unattended.replace("`${" + $script.Name + "}", $script.Value)
}

echo "Saving unattended.xml"

echo $unattended | Out-File -Encoding utf8 -FilePath "$BASE_PATH/unattended.xml"

echo "Running windows setup"

$process = Start-Process -FilePath "S:\$OS_BASE_PATH\$OS\setup.exe" -ArgumentList "/unattend:$BASE_PATH/unattended.xml /noreboot" -WindowStyle Hidden -PassThru -Wait
$process.WaitForExit()

if ($( $process.ExitCode ) -ne 0)
{
    throw "Windows setup did not complete successfully: " + $process.ExitCode
}

echo "Setup completed successfully!"

echo "copying post setup files"

$windowsDrive = ""

foreach($drive in $(Get-PSDrive -PSProvider 'FileSystem')) {
   if(Test-Path $($drive.Root + "Windows\Panther")) {
        echo "Windows drive is $drive"
		$windowsDrive = $drive.Root
		break
    }
}

if ($windowsDrive -eq "") {
	throw "Can not find Windows drive!"
}

$POST_PATH = $windowsDrive + "temp\win-net-stall"
New-Item $POST_PATH -ItemType Directory
Copy-Item -Path "$SETUP_PATH\*" -Destination "$POST_PATH\" -PassThru -Recurse
Copy-Item -Path "$BASE_PATH\*" -Destination "$POST_PATH\" -PassThru -Recurse

foreach ($script in $POST_SETUP_SCRIPTS.GetEnumerator())
{
    Get-Config "post/$script" "$POST_PATH\post\$script"
}

echo "Rebooting in 10 seconds"
Start-Sleep -Seconds 10

Ummount-Setup-Share

echo "Rebooting!"
wpeutil reboot
