$CONFIG_BASE_PATH = "win-net-stall"
$CONFIG_SERVER = "https://win-net-stall"
$TIME_SERVER = "time"


function Wait-For-Network($tries)
{
    for($try = 0; $try -lt $tries; $try++) {
        $x = gwmi -class Win32_NetworkAdapterConfiguration -filter "DHCPEnabled=TRUE" | Where { $_.DefaultIPGateway -ne $null }

        if (($x | measure).count -gt 0)
        {
            return
        }
        Start-Sleep -s 1
    }
    throw "Network unavailable after $tries tries."
}


function Run-And-Wait($path, $arguments)
{
    $process = Start-Process -FilePath $path -ArgumentList $arguments -WindowStyle Hidden -PassThru -Wait
    $process.WaitForExit()

    return $process.ExitCode
}


function Get-Config($url, $out)
{
    return Run-And-Wait "X:\Windows\system32\curl.exe" "-f -o $out $CONFIG_SERVER/$CONFIG_BASE_PATH/$url"
}


function Download-Config($addresses)
{
    foreach ($address in $addresses)
    {
        echo "Downloading config for $address"

        $exitCode = Get-Config $address "$BASE_PATH\setup_base_config.psm1"

        if ($exitCode -eq 0)
        {
            echo "Success!"
            return
        }

        echo "Failed!"
    }

    if ($( Get-Config "default" "$BASE_PATH\setup_base_config.psm1" ) -ne 0)
    {
        throw "Could not get default config data!"
    }
}

function Get-Mac-Addresses()
{
    $MacString = (Get-CimInstance win32_networkadapterconfiguration | select macaddress) | Out-String

    $Mac = @()

    foreach ($line in $MacString.Split([Environment]::NewLine))
    {
        if ( [string]::IsNullOrWhitespace($line))
        {
            continue
        }
        if ($line.StartsWith("macaddress") -or $line.StartsWith("-"))
        {
            continue
        }
        $Mac += $line
    }

    return $Mac.replace(':', '-')
}

function Run-Script($path)
{
    Import-Module $path -Force -Verbose
}

function Import-Module-Global($path)
{
    Add-Content -Path $path -Value "`nExport-ModuleMember -Function * -Alias * -Variable *`n"
    Import-Module $path -Force -Verbose -Global
}

function Include-Config($include)
{
    $path = "$BASE_PATH\$include.psm1"
    echo "Including $include ($path)"
    if (-Not$([System.IO.File]::Exists($path) ))
    {
        if ($( Get-Config $include $path ) -ne 0)
        {
            throw "Could not get $include config data!"
        }
    }
    Import-Module-Global($path)
}

function Copy-And-Include-Specific-Config()
{
    echo "Getting setup data from config (by mac address)"
    $macs = Get-Mac-Addresses

    echo "Got the following Mac Addresses: $macs"
    Download-Config($macs)

    echo "Including custom base config"

    Import-Module-Global("$BASE_PATH\setup_base_config.psm1")
}

function Mount-Setup-Share()
{
    echo "Mounting setup network share ($SETUP_BASE_PATH)"
    If (Test-Path S:)
    {
        net use S: /delete
    }
    net use S: $SETUP_BASE_PATH /user:$SETUP_USER $SETUP_PASSWORD /persistent:yes
}

function Ummount-Setup-Share()
{
    echo "Unmounting setup network share"
    net use S: /delete
}

function Run-Once($name, $command)
{
    Set-ItemProperty -Path 'HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce' -Name $name -Value $command
}

$WIN_LOGON_PATH = "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon"

function Configure-Auto-Login($domain, $user, $password)
{
    Set-ItemProperty $WIN_LOGON_PATH 'AutoLogonCount' -Value "99999" -Type String
    Set-ItemProperty $WIN_LOGON_PATH 'DefaultDomainName' -Value $domain -type String
    Set-ItemProperty $WIN_LOGON_PATH 'AutoAdminLogon' -Value "1" -Type String
    Set-ItemProperty $WIN_LOGON_PATH 'DefaultUsername' -Value $user -type String
    Set-ItemProperty $WIN_LOGON_PATH 'DefaultPassword' -Value $password -type String
}

function Remove-Auto-Login()
{
    Remove-ItemProperty $WIN_LOGON_PATH 'AutoLogonCount' -Force -ErrorAction Ignore
    Remove-ItemProperty $WIN_LOGON_PATH 'AutoAdminLogon' -Force -ErrorAction Ignore
    Remove-ItemProperty $WIN_LOGON_PATH 'DefaultUsername' -Force -ErrorAction Ignore
    Remove-ItemProperty $WIN_LOGON_PATH 'DefaultPassword' -Force -ErrorAction Ignore
    Remove-ItemProperty $WIN_LOGON_PATH 'DefaultDomainName' -Force -ErrorAction Ignore
}


function Block-Peripherals()
{
    Get-PnpDevice -Class USB | Disable-PnpDevice -Confirm:$false -ErrorAction SilentlyContinue
    Get-PnpDevice -Class Keyboard | Disable-PnpDevice -Confirm:$false -ErrorAction SilentlyContinue
    Get-PnpDevice -Class Mouse | Disable-PnpDevice -Confirm:$false -ErrorAction SilentlyContinue
}

function Enable-Peripherals()
{
    Get-PnpDevice -Class USB | Enable-PnpDevice -Confirm:$false -ErrorAction SilentlyContinue
    Get-PnpDevice -Class Keyboard | Enable-PnpDevice -Confirm:$false -ErrorAction SilentlyContinue
    Get-PnpDevice -Class Mouse | Enable-PnpDevice -Confirm:$false -ErrorAction SilentlyContinue
}
