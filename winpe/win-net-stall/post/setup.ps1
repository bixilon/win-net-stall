echo "Win-Net-Stall Post Setup 1.0"
$ErrorActionPreference = "Stop"

$BASE_PATH = "C:\temp\win-net-stall"
$SKIP_FILE = "$BASE_PATH\script_index.txt"

Import-Module "$BASE_PATH\include\*.ps1" -Force -Verbose -Global

echo "Waiting for network"

Wait-For-Network(20)

echo "Including configs"
$global:POST_SETUP_SCRIPTS = @() # config will add scripts to it
Import-Module-Global("$BASE_PATH\setup_base_config.psm1")

if ($BLOCK_PERIPHERALS)
{
    Block-Peripherals
}

echo "Mounting setup share"
Mount-Setup-Share

echo "Running post setup"

$scriptIndex = 0


$skipUntil = 0
if ($([System.IO.File]::Exists($SKIP_FILE) ))
{
    $string = Get-Content -Path $SKIP_FILE
    $skipUntil = [int]$string
}


function Request-Reboot()
{
    echo "Rebooting in 10 seconds..."
    Set-Content -Path $SKIP_FILE -Force -Value $( $scriptIndex + 1 )
    Configure-Auto-Login $DEVICE_NAME "$DEVICE_NAME\Administrator" $LOCAL_PASSWORD
    Run-Once "win-net-stall" "$BASE_PATH\post\setup.cmd"
    Start-Sleep -s 10
    Restart-Computer -Force
    Start-Sleep -s 100 # not really needed
    exit
}

foreach ($script in $global:POST_SETUP_SCRIPTS.GetEnumerator())
{
    if ($scriptIndex -ge $skipUntil)
    {
        echo "Running $script"
        Run-Script("$BASE_PATH\post\$script")
    }
    $scriptIndex = $scriptIndex + 1
}

echo "Post setup done! Cleaning up and rebooting..."

if ($BLOCK_PERIPHERALS)
{
    Enable-Peripherals
}

Start-Sleep -s 10
if ( [string]::IsNullOrWhitespace($OPERATING_USER_PASSWORD))
{
    Remove-Auto-Login
}
Remove-Item $BASE_PATH -Recurse -Force
Restart-Computer -Force
