# Win Net Stall
(Short for Windows Network Install)

# Prerequisites

- Windows Server or samba server (for hosting files, like windows setup or `unattended.xml` files)
- PXE Boot environment (to start windows pe)
- Web server (e.g. `nginx` to host device configurations)
- [domain join user](https://www.prajwaldesai.com/allow-domain-user-to-add-computer-to-domain/)

# Setup

1. Create a `setup` network share
2. Get Windows ISOs
3. Build WinPE
4. Configure server
5. Create device configs
6. Create `unatteded.xml` files
7. Reinstall your devices :)

## `setup` network share

1. Create a network share (e.g. `share`) on your server
2. Create a user (like `setup`) that has no power
3. Allow **read** access to the network share to that user (deny every other user, but this does not really improve security, the password for the user will be written down in a script in plaintext)
4. Restrict login to the user (e.g. on what workstations, ...)

## Windows ISOs

1. Download the iso(s) for your devices ([Windows 8.1 ISO](https://www.microsoft.com/de-de/software-download/windows8ISO), [Windows 10 ISO](https://www.microsoft.com/de-de/software-download/windows10ISO), [Windows 11 ISO](https://www.microsoft.com/de-de/software-download/windows11)).
2. Extract the iso(s) to a sub folder of your network share (like `\\server\setup\os\Win10_German_x64_21H2` or `\\server\setup\os\Win11_German_x64v1`). In this folder must be a setup.exe
3. Optional: Delete the `boot.wim` file in the `sources` folder to free up space (We won't need that file anymore, ~400MB)

## Building windows pe

1. Install [Windows ADK](https://docs.microsoft.com/en-us/windows-hardware/manufacture/desktop/download-winpe--windows-pe?view=windows-11)
2. Install the Windows PE addon
3. Prepare windows pe environment (`copype amd64 C:\WinPE_amd64`)
4. Mount image: `Dism /Mount-Image /ImageFile:"C:\WinPE_amd64\media\sources\boot.wim" /Index:1 /MountDir:"C:\WinPE_amd64\mount"`
5. [Install powershell](https://docs.microsoft.com/en-us/windows-hardware/manufacture/desktop/winpe-adding-powershell-support-to-windows-pe?view=windows-11):
```
Dism /Add-Package /Image:"C:\WinPE_amd64\mount" /PackagePath:"C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\WinPE-WMI.cab"
Dism /Add-Package /Image:"C:\WinPE_amd64\mount" /PackagePath:"C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\en-us\WinPE-WMI_en-us.cab"
Dism /Add-Package /Image:"C:\WinPE_amd64\mount" /PackagePath:"C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\WinPE-NetFX.cab"
Dism /Add-Package /Image:"C:\WinPE_amd64\mount" /PackagePath:"C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\en-us\WinPE-NetFX_en-us.cab"
Dism /Add-Package /Image:"C:\WinPE_amd64\mount" /PackagePath:"C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\WinPE-Scripting.cab"
Dism /Add-Package /Image:"C:\WinPE_amd64\mount" /PackagePath:"C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\en-us\WinPE-Scripting_en-us.cab"
Dism /Add-Package /Image:"C:\WinPE_amd64\mount" /PackagePath:"C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\WinPE-PowerShell.cab"
Dism /Add-Package /Image:"C:\WinPE_amd64\mount" /PackagePath:"C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\en-us\WinPE-PowerShell_en-us.cab"
Dism /Add-Package /Image:"C:\WinPE_amd64\mount" /PackagePath:"C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\WinPE-StorageWMI.cab"
Dism /Add-Package /Image:"C:\WinPE_amd64\mount" /PackagePath:"C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\en-us\WinPE-StorageWMI_en-us.cab"
Dism /Add-Package /Image:"C:\WinPE_amd64\mount" /PackagePath:"C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\WinPE-DismCmdlets.cab"
Dism /Add-Package /Image:"C:\WinPE_amd64\mount" /PackagePath:"C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\en-us\WinPE-DismCmdlets_en-us.cab"
```
6. (For Windows 11) [install necessary packages](https://docs.microsoft.com/en-us/windows-hardware/manufacture/desktop/winpe-add-packages--optional-components-reference?view=windows-11#winpe-optional-components--):
```
Dism /Add-Package /Image:"C:\WinPE_amd64\mount" /PackagePath:"C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\WinPE-SecureStartup.cab"
Dism /Add-Package /Image:"C:\WinPE_amd64\mount" /PackagePath:"C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\en-us\WinPE-SecureStartup_en-us.cab"

Dism /Add-Package /Image:"C:\WinPE_amd64\mount" /PackagePath:"C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Windows Preinstallation Environment\amd64\WinPE_OCs\WinPE-SecureBootCmdlets.cab"
```
7. Copy the files from the winpe folder (from that repository) to `C:\WinPE_amd64\mount` (replace files if needed)
8. Modify `C:\WinPE_amd64\mount\win-net-stall\include\include.ps1`, `C:\WinPE_amd64\mount\win-net-stall\pe.ps1` and `C:\WinPE_amd64\mount\win-net-stall\post.ps1` to your needs
9. Make additional adjustments (to your windows pe) as you need it
   - install software
   - add drivers
   - customize design
   - add your own certificate authority
10. Unmount image: `Dism /Unmount-Image /MountDir:C:\WinPE_amd64\mount /Commit`
11. Create iso: `makewinpemedia /iso C:\WinPE_amd64 C:\WinPE_amd64\winpe_amd64.iso`
12. Configure your pxe (probably iPXE or pxelinux) to be able to boot that iso file

It is recommended to create a private fork of the project (at least I am doing this)

## Configure your server

In `C:\WinPE_amd64\mount\setup\include.ps1` are 2 host names (`$CONFIG_SERVER` and `$TIME_SERVER`). Make sure they are resolvable or replace them with your servers ip addresses

## Create device configs

The script will try to get a load a custom configuration script from the web server based on the device mac address(es) (e.g. `https://win-net-stall/setup/config/AA-BB-CC-DD-EE-FF`). Make sure that file exists. You can find a documented example configuration in this repository. You can straight copy the files in the `web` folder to your web server. You can create devices groups, include other configs, run custom PowerShell code or do whatever you want. It is completely documented.

## Create `unattended.xml` files

In order to install windows silently, you have to create `unattended.xml` files for your devices. The file(s) need to be on you samba share in the `\\server\setup\unattend\` folder. You can find an example file in the repository. You can configure that path and the file name per device (group) in your device configuration. The format is well documented at [microsoft.com](https://docs.microsoft.com/de-de/windows-hardware/customize/desktop/unattend/). There is also an example file under [/setup/unattended/default.xml](/setup/unattended/default.xml). Feel free to use it. The provided `unattended.xml` is made for Windows 8.1+. It does not work for windows 7 and below.

The most important thing is, that you execute the post setup script as (probably only) FirstLogonScript. See post setup for more details

Note: You can use variables in the file in the format ${NAME}. Those variables can be defined in you device configs. The only requirement is, that they must be written fully in caps (underscores and numbers allowed)

## Post setup

Post setup is the setup routine that gets executed as soon as the device is fully installed. There are some predefined scripts in [web/setup/config/post](web/win-net-stall/post). Every action should be in its own script for structuring purposes. The device config and all other win-net-stall includes are included. The setup share (`S:`) is also mounted. You should do actions like:

- renaming device (`unattended.xml` is sometimes buggy)
- joining the domain (again, also buggy in `unattended.xml`)
- remove preinstalled apps
- do one time system modifications
- install drivers
- install your software deployment tool
- enable bitlocker with tpm (if possible)
- **Disable the local admin account** (really advisable; must be the last action)

Every action can reboot the device. Don't reboot the device with the `shutdown` command, use `Request-Reboot`.
`Request-Reboot` saves the current state of the post setup, configures auto start and auto login.

## Reinstall your devices :)

Once everything is installed and set up, test it and feel free to reinstall devices :)
