# Global include template

# Domain to use for setup
$DOMAIN = "MYDOMAIN"
$DOMAIN_FQDN = "MYDOMAIN.MUC" # this is used for the domain join, mostly the $DOMAIN it self is not resolved.

# Share where the windows setup and the unattended files are
# Folder will be mounted as S:
$SETUP_BASE_PATH = "\\server\setup"
# Use that needs read access to that share
$SETUP_USER = "$DOMAIN\setup"
# Password for the user above
$SETUP_PASSWORD = "password"

# files for the unattended files
$UNATTENDED_BASE_PATH = "unattended"
# unattended file to use
$UNATTENDED_PATH = "default.xml"

# folder where the folder to the extracted windows iso can be found
$OS_BASE_PATH = "os"

# user for domain join
# This user should not be allowed to login on any device
# This user should be maximum restricted
# The only power that it should have is to modify (or create) computer ldap objects
$DOMAIN_USER = "$DOMAIN\domainjoin"
# password for the domain join user
$DOMAIN_PASSWORD = "password"

# password that will be used to access the "Administrator" user
# Can be anything, the user is going to be disabled (after post setup)
$LOCAL_PASSWORD = "password"

# product key to install it
# nothing leaked here, this is a generic (windows 10 and 11) product key, it can not be used to activate windows
# In ad dc environment you should use your own license server (MAK or KMS)
$PRODUCT_KEY = "VK7JG-NPHTM-C97JM-9MPGT-3V66T"

$BLOCK_PERIPHERALS = $false

# those scripts will be executed in the post setup stage in order
$global:POST_SETUP_SCRIPTS += "disable_standby.ps1"
$global:POST_SETUP_SCRIPTS += "rename_device.ps1"
$global:POST_SETUP_SCRIPTS += "enable_bitlocker.ps1"
$global:POST_SETUP_SCRIPTS += "remove_installed_apps.ps1"
$global:POST_SETUP_SCRIPTS += "install_windows_updates.ps1"
$global:POST_SETUP_SCRIPTS += "install_windows_updates.ps1" # after updates are installed, there is the possibility that windows finds more updates...
$global:POST_SETUP_SCRIPTS += "join_samba_domain.ps1"
$global:POST_SETUP_SCRIPTS += "update_gpos.ps1"
$global:POST_SETUP_SCRIPTS += "update_gpos.ps1" # update gpos again with a reboot, some stuff just needs a reboot and then we want to force apply them again
$global:POST_SETUP_SCRIPTS += "enable_standby.ps1"
$global:POST_SETUP_SCRIPTS += "disable_local_admin.ps1"
$global:POST_SETUP_SCRIPTS += "set_last_username.ps1"
