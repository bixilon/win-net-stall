# We want to include the global (default) settings
# We will just overwrite settings here
Include-Config("default.psm1")

# Special device group
# We are using an unattended.xml specific to this group
# (overwrite from default profile)
$UNATTENDED_PATH = "group.xml"

# The os (or better the folder under $OS_BASE_PATH) we want to install
# The iso must be extracted
# There must be a setup.exe file in there
$OS = "Win11_German_x64v1"
