# This script joins an ad-dc domain

$password = $DOMAIN_PASSWORD | ConvertTo-SecureString -AsPlainText -Force
$credentials = New-Object System.Management.Automation.PSCredential($DOMAIN_USER, $password)

echo "Joining domain $DOMAIN with user $DOMAIN_USER"

Add-Computer -DomainName $DOMAIN_FQDN -Credential $credentials -ErrorAction Stop -PassThru -Verbose

Start-Sleep -Seconds 10

echo "Updating gpos..."
echo n | gpupdate /force /wait:0

Request-Reboot
