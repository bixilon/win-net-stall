# This enabled bitlocker for the device

# thanks https://community.spiceworks.com/topic/1972369-powershell-script-to-enable-bitlocker
$TPM = Get-WmiObject win32_tpm -Namespace root\cimv2\security\microsofttpm | where { $_.IsEnabled().Isenabled -eq 'True' } -ErrorAction SilentlyContinue
$WindowsVer = Get-WmiObject -Query 'select * from Win32_OperatingSystem where (Version like "6.2%" or Version like "6.3%" or Version like "10.0%") and ProductType = "1"' -ErrorAction SilentlyContinue
$BitLockerReadyDrive = Get-BitLockerVolume -MountPoint $env:SystemDrive -ErrorAction SilentlyContinue


# if tpm is present, windows supports it and the system drive supports tpm, enable it
if ($WindowsVer -and $TPM -and $BitLockerReadyDrive)
{
    Clear-Tpm  -ErrorAction SilentlyContinue
    Initialize-Tpm -AllowCLear -AllowPhysicalPresence -ErrorAction SilentlyContinue
    Enable-BitLocker -MountPoint $env:SystemDrive -EncryptionMethod Aes128 -TpmProtector -ErrorAction SilentlyContinue
    Request-Reboot
}
