# Automatically installs all windows updates

$ErrorActionPreference = "Continue"
Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
Install-Module PSWindowsUpdate -Force

Get-WindowsUpdate
Install-WindowsUpdate -MicrosoftUpdate -AcceptAll -IgnoreReboot

$ErrorActionPreference = "Stop"
Request-Reboot
