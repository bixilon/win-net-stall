# This script renames the local device and reboots

Rename-Computer -NewName $DEVICE_NAME -Force

echo "Device got renamed successfully"

Request-Reboot
