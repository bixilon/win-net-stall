# Disable local administrator account
# The password "can be read out in plain text"
# We don't want that a user with such power is accessible to users who took a look into this system
net user Administrator /active:no
net user Install /active:no
