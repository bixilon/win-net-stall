# Not that bad if the uninstall fails, most of the windows apps are not possible to uninstall
$ErrorActionPreference = "Continue"

# Remove all preinstalled windows apps (normally you don't want them im ad-dc environments)
Get-AppxPackage -AllUsers * | Remove-AppxPackage
Get-AppxPackage * | Remove-AppxPackage
Get-AppxProvisionedPackage -online | Remove-AppxProvisionedPackage -online

### OneDrive
echo "Removing OneDrive"
taskkill /f /im OneDrive.exe
Run-And-Wait "$Env:SystemRoot\SysWOW64\OneDriveSetup.exe" "/uninstall"

### Edge
echo "Removing Microsoft Edge"

taskkill /f /im msedge.exe
$uninstallerPath = $( Get-ChildItem -Path "C:\Program Files (x86)\Microsoft\Edge\Application\" -Recurse -Directory -Force -ErrorAction SilentlyContinue | Select-Object FullName | where{ $_ -like "*Installer*" } ).FullName
Run-And-Wait "$uninstallerPath\setup.exe" "--uninstall --system-level --verbose-logging --force-uninstall"
Remove-Item "C:\Users\Public\Desktop\Microsoft Edge.lnk" -Force

$uninstallerPath = $( Get-ChildItem -Path "C:\Program Files (x86)\Microsoft\EdgeCore\" -Recurse -Directory -Force -ErrorAction SilentlyContinue | Select-Object FullName | where{ $_ -like "*Installer*" } ).FullName
Run-And-Wait "$uninstallerPath\setup.exe" "--uninstall --system-level --verbose-logging --force-uninstall"
Remove-Item "C:\Users\Public\Desktop\Microsoft Edge.lnk" -Force


$ErrorActionPreference = "Stop"

Request-Reboot
