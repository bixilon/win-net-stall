# This script joins an ad-dc domain
# Important: samba is not compatible with the power shell commands (e.g. Add-Computer). This script workarounds this limitation.

# Add-WindowsCapability -Online -Name Rsat.ActiveDirectory.DS-LDS.Tools
# netdom join %computername% /domain:$DOMAIN_FQDN /UserD:$DOMAIN\$DOMAIN_USER /PasswordD:$DOMAIN_PASSWORD

# wmic computersystem where name="%computername%" call joindomainorworkgroup fjoinoptions=3 name="homelab.local" username="homelab\labadmin" Password="secret"
function Join-Domain() {
	(Get-WMIObject -NameSpace "Root\Cimv2" -Class "Win32_ComputerSystem").JoinDomainOrWorkgroup($DOMAIN_FQDN, $DOMAIN_PASSWORD, "$DOMAIN\$DOMAIN_USER", $null, 3)
}

Join-Domain

while ($(Get-WmiObject -Namespace "Root\Cimv2" -Class "Win32_ComputerSystem").Domain -ne $DOMAIN_FQDN.ToLower()) {
	echo "Domain join seems to have failed!. Trying again in 5 seconds"
	Start-Sleep -s 5
	Join-Domain
}
